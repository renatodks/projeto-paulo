package InterfaceLZ;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;













import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.SwingConstants;

import DaoLZ.Daodados;
import ModeloLZ.Cadastro;

public class PesquisaCadastro extends JFrame {

	private JPanel contentPane;
	private JTextField txtnome;
	private JTextField txtcpf;
	private JTextField txtdatanasc;
	private JTextField txtinicio;
	private JTextField txtobjetivo;
	private JTextField txtmensalidade;
	

	private JTextField txttreino;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					String tema = "com.jtattoo.plaf.hifi.HiFiLookAndFeel";

		            // AQUI VC SETA O LOOK AND FEEL
		            UIManager.setLookAndFeel(tema);
		            JOptionPane.showMessageDialog(null, "LookAndFell DARK !!");
		           
					PesquisaCadastro frame = new PesquisaCadastro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PesquisaCadastro() {
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Academia CHERNOBYL");
		lblNewLabel.setForeground(new Color(34, 139, 34));
		lblNewLabel.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 45));
		lblNewLabel.setBounds(20, 11, 512, 48);
		contentPane.add(lblNewLabel);
		
		JLabel lblOndeOsMonstros = new JLabel("Onde os monstros s\u00E3o criados !");
		lblOndeOsMonstros.setForeground(new Color(34, 139, 34));
		lblOndeOsMonstros.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 20));
		lblOndeOsMonstros.setBounds(30, 56, 324, 48);
		contentPane.add(lblOndeOsMonstros);
		
		JButton btnCadastrar = new JButton("Alterar Cadastro/Treino");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String nome = txtnome.getText();
				String cpf = txtcpf.getText();
				String datanasc = txtdatanasc.getText();
				String objetivo = txtobjetivo.getText();
				String treino = txttreino.getText();
				String datai = txtinicio.getText();
				String mensalidade = txtmensalidade.getText();
				
				Cadastro c = new Cadastro();			
				c.setNome(nome);
				c.setCpf(cpf);
				c.setDatanasc(datanasc);
				c.setObjetivo(objetivo);
				c.setTreino(treino);
				c.setDatai(datai);
				c.setMensalidade(mensalidade);
				
				
				Daodados altera = new Daodados();
				
				altera.altera(c);
				
				JOptionPane.showMessageDialog(null, "Alterado !!!");
				
				
				
			}
		});
		btnCadastrar.setForeground(new Color(0, 0, 0));
		btnCadastrar.setBackground(new Color(0, 128, 0));
		btnCadastrar.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 17));
		btnCadastrar.setBounds(512, 532, 272, 30);
		contentPane.add(btnCadastrar);
		
		txtnome = new JTextField();
		txtnome.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 11));
		txtnome.setForeground(new Color(255, 255, 255));
		txtnome.setBounds(10, 133, 324, 30);
		contentPane.add(txtnome);
		txtnome.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel_1.setBounds(20, 91, 89, 48);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setForeground(Color.WHITE);
		lblCpf.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 30));
		lblCpf.setBounds(20, 174, 89, 48);
		contentPane.add(lblCpf);
		
		txtcpf = new JTextField();
		txtcpf.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 11));
		txtcpf.setText("Digite o CPF e clique em buscar para achar seu cadastro");
		txtcpf.setForeground(Color.BLACK);
		txtcpf.setColumns(10);
		txtcpf.setBounds(10, 213, 324, 30);
		contentPane.add(txtcpf);
		
		JLabel lblDataDe = new JLabel("Data de Nascimento");
		lblDataDe.setForeground(Color.WHITE);
		lblDataDe.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 30));
		lblDataDe.setBounds(10, 254, 302, 48);
		contentPane.add(lblDataDe);
		
		txtdatanasc = new JTextField();
		txtdatanasc.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 11));
		txtdatanasc.setForeground(Color.WHITE);
		txtdatanasc.setColumns(10);
		txtdatanasc.setBounds(10, 301, 324, 30);
		contentPane.add(txtdatanasc);
		
		JLabel lblDataDeInicio = new JLabel("Data de Inicio");
		lblDataDeInicio.setForeground(Color.WHITE);
		lblDataDeInicio.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 30));
		lblDataDeInicio.setBounds(460, 254, 302, 48);
		contentPane.add(lblDataDeInicio);
		
		txtinicio = new JTextField();
		txtinicio.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 11));
		txtinicio.setForeground(Color.WHITE);
		txtinicio.setColumns(10);
		txtinicio.setBounds(450, 301, 324, 30);
		contentPane.add(txtinicio);
		
		JLabel lblObjeto = new JLabel("Objetivo");
		lblObjeto.setForeground(Color.WHITE);
		lblObjeto.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 30));
		lblObjeto.setBounds(450, 91, 126, 48);
		contentPane.add(lblObjeto);
		
		txtobjetivo = new JTextField();
		txtobjetivo.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 11));
		txtobjetivo.setForeground(Color.WHITE);
		txtobjetivo.setColumns(10);
		txtobjetivo.setBounds(450, 133, 324, 30);
		contentPane.add(txtobjetivo);
		
		JLabel lblMensalidade = new JLabel("Mensalidade");
		lblMensalidade.setForeground(Color.WHITE);
		lblMensalidade.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 30));
		lblMensalidade.setBounds(460, 174, 191, 48);
		contentPane.add(lblMensalidade);
		
		txtmensalidade = new JTextField();
		txtmensalidade.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 11));
		txtmensalidade.setForeground(Color.WHITE);
		txtmensalidade.setColumns(10);
		txtmensalidade.setBounds(450, 213, 324, 30);
		contentPane.add(txtmensalidade);
		
		txttreino = new JTextField();
		txttreino.setHorizontalAlignment(SwingConstants.CENTER);
		txttreino.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 12));
		txttreino.setBounds(10, 342, 764, 187);
		contentPane.add(txttreino);
		txttreino.setColumns(10);
		
		JButton btnBuscarCadastro = new JButton("Buscar");
		btnBuscarCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Daodados dao = new Daodados();

				ArrayList<Cadastro> lista = new ArrayList<Cadastro>();

				lista = dao.lista(txtcpf.getText());
                    
                    if (lista.size() == 0)
                    {
                    	JOptionPane.showMessageDialog(null, "Cpf n�o Encontrado");
                    	
                    	return;
                    }
                    else
                    {
                    txtcpf.setText(lista.get(0).getCpf());
                    txtnome.setText(lista.get(0).getNome());  
                    txtobjetivo.setText(lista.get(0).getObjetivo());
                    txtinicio.setText(lista.get(0).getDatai());
                    txtmensalidade.setText(lista.get(0).getMensalidade());
                    txttreino.setText(lista.get(0).getTreino());
                    txtdatanasc.setText(lista.get(0).getDatanasc());
                    
                    }


				
			}}
		);
		btnBuscarCadastro.setForeground(Color.BLACK);
		btnBuscarCadastro.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 17));
		btnBuscarCadastro.setBackground(new Color(0, 128, 0));
		btnBuscarCadastro.setBounds(339, 211, 101, 30);
		contentPane.add(btnBuscarCadastro);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(PesquisaCadastro.class.getResource("/IMG/S\u00EDmbolo verde de peligro biol\u00F3gico (Biohazard).jpg")));
		label.setForeground(Color.WHITE);
		label.setBounds(0, 0, 784, 562);
		contentPane.add(label);
	}
}
