package InterfaceLZ;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Dialog.ModalExclusionType;

import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JMenuBar;
import javax.swing.JButton;

import java.awt.Insets;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUIinicial extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					String tema = "com.jtattoo.plaf.hifi.HiFiLookAndFeel";

		            // AQUI VC SETA O LOOK AND FEEL
		            UIManager.setLookAndFeel(tema);
		            GUIinicial frame = new GUIinicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUIinicial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 17));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.BLACK);
		menuBar.setMargin(new Insets(3, 3, 3, 3));
		menuBar.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 17));
		contentPane.add(menuBar, BorderLayout.NORTH);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		mnCadastro.setBackground(Color.BLACK);
		mnCadastro.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 17));
		menuBar.add(mnCadastro);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Cadastrar");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				CadastroGUI c = new CadastroGUI();
				c.setVisible(true);	
			}
		});
		mntmNewMenuItem.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 17));
		mntmNewMenuItem.setBackground(new Color(0, 128, 0));
		mnCadastro.add(mntmNewMenuItem);
		
		JMenu mnNewMenu_1 = new JMenu("Pesquisa");
		mnNewMenu_1.setBackground(Color.BLACK);
		mnNewMenu_1.setFont(new Font("DejaVu Sans Condensed", Font.BOLD | Font.ITALIC, 17));
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Pesquisar/Alterar Cadastro");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				
				PesquisaCadastro f = new PesquisaCadastro();
				f.setVisible(true);
				
			}
		});
		mntmNewMenuItem_1.setBackground(new Color(0, 128, 0));
		mntmNewMenuItem_1.setFont(new Font("DejaVu Serif Condensed", Font.BOLD | Font.ITALIC, 17));
		mnNewMenu_1.add(mntmNewMenuItem_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(GUIinicial.class.getResource("/IMG/simbolo.jpg")));
		contentPane.add(lblNewLabel);
	}
}
