package DaoLZ;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConFactory {
	public static final int MYSQL = 0;
    private static final String 
    MySQLDriver = "com.mysql.jdbc.Driver";

    /**
     * Realiza conexão no banco de dados 
     * @param url: Qual o local do banco de dados ?
     * @param user: Qual o usuário do banco de dados?
     * @param password: Qual o nome do banco de dados ?
     * @param banco: Informe 0 para banco de dados MYSQL
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @return Uma conexão com o banco de dados, caso nenhum erro ocorra no processo.
     */
    public static Connection conexao(String url, 
    		String user, String password,
          int banco) throws ClassNotFoundException, 
    SQLException {
    	
       switch (banco) {
       case MYSQL:
          Class.forName(MySQLDriver);
          break;
       }
       return DriverManager.getConnection(url, user, 
    		   password);
    }
    
}
