package DaoLZ;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;





import ModeloLZ.Cadastro;





public class Daodados {

	// Configura essas vari�veis de acordo com o seu banco
	private final String URL = "jdbc:mysql://localhost/bdlz",
			USER = "root", 
			PASSWORD = "ifsuldeminas";

	private Connection con;
	private Statement comando;

	private void conectar() {
		try {
			con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
			comando = con.createStatement();
			System.out.println("Conectado!");
		} catch (ClassNotFoundException e) {
			imprimeErro("Erro ao carregar o driver", e.getMessage());
		} catch (SQLException e) {
			imprimeErro("Erro ao conectar", e.getMessage());
		}
	}

	private void fechar() {
		try {
			comando.close();
			con.close();
			System.out.println("Conex�o Fechada");
		} catch (SQLException e) {
			imprimeErro("Erro ao fechar conex�o", e.getMessage());
		}
	}

	private void imprimeErro(String msg, String msgErro) {
		JOptionPane.showMessageDialog(null, msg, "Erro critico", 0);
		System.err.println(msg);
		System.out.println(msgErro);
		System.exit(0);
	}

	public void insere(Cadastro l){
		conectar();
		PreparedStatement insertcliente = null;

		try {
			//        	 String sql = "INSERT INTO Cadastro VALUES('"
			//                     + f.getCodigoCnpj() + "', '" + f.getRazaoSocial() + "',"
			//                     + f.getEndereco() + ",'" + f.getTelefone() + "')";
			//        	System.out.println("Sql: " + sql);


			String sql = "INSERT INTO cadastro VALUES(?,?,?,?,?,?,?)";
			insertcliente = con.prepareStatement(sql);
			insertcliente.setString(1, l.getCpf());
			insertcliente.setString(2, l.getNome());
			insertcliente.setString(3, l.getDatanasc());
			insertcliente.setString(4, l.getObjetivo());
			insertcliente.setString(5, l.getMensalidade());
			insertcliente.setString(6, l.getDatai());		
			insertcliente.setString(7, l.getTreino());

			int r=insertcliente.executeUpdate();

			if(r > 0){
				//comando.executeUpdate(sql);
				System.out.println("Dados Inseridos!");
			}
		} catch (SQLException e) {
			imprimeErro("Erro ao inserir dados", e.getMessage());
		} finally {
			fechar();
		}
	}

	public void altera(Cadastro l){
		conectar();
		PreparedStatement alterarFornecedor = null;
		
		try {
		
		String sql = "UPDATE Cadastro "
				+ "SET Nome=?,Datanasc=?,objetivo=?,Mensalidade=?,datai=?,Treino=?"
				+ "WHERE Cpf = '?' ";
		
		alterarFornecedor = con.prepareStatement(sql);
		alterarFornecedor.setString(1, l.getNome());
		alterarFornecedor.setString(2, l.getDatanasc());
		alterarFornecedor.setString(3, l.getObjetivo());
		alterarFornecedor.setString(4, l.getMensalidade());
		alterarFornecedor.setString(5, l.getDatai());		
		alterarFornecedor.setString(6, l.getTreino());

		int r=alterarFornecedor.executeUpdate();


		if(r > 0){
			//comando.executeUpdate(sql);
			System.out.println("Alterado!");
		}
		
		}catch(SQLException e){
			imprimeErro("Erro ao alterar Cadastro", e.getMessage());
		}
	     finally {
		    fechar();
	    }
		
	}



public ArrayList<Cadastro> lista(String text) {
	// TODO Auto-generated method stub
	conectar();
	ArrayList<Cadastro> lista  = new ArrayList<Cadastro>();
	try {
	
	PreparedStatement selecionarFornecedor = null;
	ResultSet rs = null;
	
	String sql = "SELECT * FROM cadastro WHERE Cpf LIKE '" + text + "%'";
	
	
	selecionarFornecedor = con.prepareStatement(sql);
	
	rs = selecionarFornecedor.executeQuery(sql);
	
	while(rs.next()){
		Cadastro t = new Cadastro();
		t.setCpf(rs.getString("Cpf"));
		t.setNome(rs.getString("Nome"));
		t.setMensalidade(rs.getString("Mensalidade"));
		t.setDatai(rs.getString("Datai"));
		t.setDatanasc(rs.getString("Datanasc"));
		t.setObjetivo(rs.getString("Objetivo"));
		t.setTreino(rs.getString("Treino"));
		
		lista.add(t);
	}
	
	} catch(SQLException e){
		imprimeErro("Erro ao selecionar Aluno", 
				e.getMessage());
	}finally {
	    fechar();
    }
	
	return lista;
}



}



